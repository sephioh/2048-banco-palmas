const fs = require('fs');
const file_encoding = 'utf8';

module.exports = {

    parse_raw_data_to_grid: function(raw_data){
        var string1 = raw_data.split('\n');
        var matrix = [];
        for(var i=0; i< string1.length;i++){
            matrix.push(string1[i].split(' '));
        }
        return matrix;
    }
};
