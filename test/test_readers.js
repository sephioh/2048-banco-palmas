"use strict";

const assert = require('assert');
const readers = require('../src/readers');
const it = require("mocha").it;
const describe = require("mocha").describe;


describe('readers.js', function() {
    describe('read_file', function() {
        it('should read file content', function() {
            let file_content = readers.read_file('test/resources/input.txt');
            let expected_file_content = '0 0 0 2\n2 0 0 2\n2 0 0 2\n2 0 0 2';
            assert.equal(file_content, expected_file_content);
        });
    });
});
