"use strict";
var parse_raw_data_to_grid = require("./src/parsers.js").parse_raw_data_to_grid;
const read_input_text_parameter = require("./src/readers.js").read_input_file_parameter;
const read_file = require("./src/readers.js").read_file;


let raw_data = read_file(read_input_text_parameter());
let grid = parse_raw_data_to_grid(raw_data);
// let new_grid = move_left(grid);
// display(new_grid);
